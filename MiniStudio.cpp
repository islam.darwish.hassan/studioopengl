// MiniStudio.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#define STB_IMAGE_IMPLEMENTATION

#include <iostream>

#include <stdio.h>
#include <string.h>
#include <cmath>
#include <vector>

#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "Window.h"
#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"
#include "Texture.h"


const float toRadius = 3.14159265f / 180.0f; //to be used to convert from degree to radius 
Window mainWindow;
std::vector<Mesh*> meshList;
std::vector<Shader> shaderList;
Camera camera;
Texture brickTexture;
Texture dirtTexture;
GLfloat deltaTime = 0.0f;
GLfloat lastTime = 0.0f;

// Vertex Shader
static const char* vShader = "Shaders/shader.vert";

// Fragment Shader
static const char* fShader = "Shaders/shader.frag";


//translation
bool direction = true;
float triOffset = 0.0f;
float triMaxoffset = 0.7f;
float triIncrement = 0.005f;
float currentAngle = 0.0f;

void CreateMeshes() {
	//define indices for graphoc card 
	unsigned int indices[] = {
		//till graphic card what  points to be used to create each triangle to build a prymaid 
		0, 3, 1,
		1, 3, 2,
		2, 3, 0,
		0, 1, 2
	};
	//define each vertix of the triangle
	GLfloat vertices[] = {
		//	x      y      z			u	  v
		-1.0f, -1.0f, 0.0f,		0.0f, 0.0f,
		0.0f, -1.0f, 1.0f,		0.5f, 0.0f,
		1.0f, -1.0f, 0.0f,		1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.5f, 1.0f
	};
	Mesh* obj1 = new Mesh();
	obj1->CreateMesh(vertices, indices, 20, 12);
	meshList.push_back(obj1);

	Mesh* obj2 = new Mesh();
	obj2->CreateMesh(vertices, indices, 20, 12);
	meshList.push_back(obj2);


}
void CreateShaders()
{
	Shader* shader1 = new Shader();
	shader1->CreateFromFiles(vShader, fShader);
	shaderList.push_back(*shader1);
}

int main()
{
	//init window
	mainWindow = Window(800, 600);
	mainWindow.Initialise();

	//init meshes
	CreateMeshes();
	//init shaders
	CreateShaders();
	//define models location 
	camera = Camera(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), -90.0f, 0.0f, 5.0f, 0.5f);



	brickTexture = Texture("Textures/brick.png");
	brickTexture.LoadTexture();
	dirtTexture = Texture("Textures/dirt.png");
	dirtTexture.LoadTexture();
	GLuint uniformProjection = 0, uniformModel = 0, uniformView = 0;;


	//creating mat4 projection for one time (field of view of top to bottom ,aspect ratio,near view ,far view )
	glm::mat4 projection = glm::perspective(45.0f, (GLfloat)mainWindow.getBufferWidth() / mainWindow.getBufferHeight(), 0.1f, 100.0f);


	//Loop until window closed from close button
	while (!mainWindow.getShouldClose())
	{
		GLfloat now = glfwGetTime(); // SDL_GetPerformanceCounter();
		deltaTime = now - lastTime; // (now - lastTime)*1000/SDL_GetPerformanceFrequency();
		lastTime = now;

		//get and handle inputs events 
		glfwPollEvents();
		// Clear the window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//camera.keyControl(mainWindow.getsKeys(), deltaTime);
		///camera.mouseControl(mainWindow.getXChange(), mainWindow.getYChange());

		//checking direction of triangle
		if (direction) {
			//if we are heading to x we keep incrementing values 
			triOffset += triIncrement;
		}
		else {
			//if we heading to left subscrbting from it 
			triOffset -= triIncrement;

		}
		// check if it above or below max value 
		if (abs(triOffset) >= triMaxoffset)
		{//change direction
			direction = !direction;
		}
		currentAngle += 1.0f;
		if (currentAngle >= 360) 
		{
			currentAngle -= 360;
		}

		//Clear window 
		//what color we want screen to clear to 
		glClearColor(0.0f, 0.0f, 0.0f,1.0f);
		//to clear color buffer in other case we may clear depth buffer for example and clearing depth buffer 
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		shaderList[0].UseShader();
		uniformModel = shaderList[0].GetModelLocation();
		uniformProjection = shaderList[0].GetProjectionLocation();
		uniformView = shaderList[0].GetViewLocation();

		//creating mat4 model
		glm::mat4 model = glm::mat4(1.0);
		//adding 3 values we want to translate with 
		model = glm::translate(model, glm::vec3(triOffset, 0.0f, 0.0f));
		//translate model in z axis to actually see it  
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.5f));

		// to rotate the model with angle and rotating axis
		model = glm::rotate(model, currentAngle * toRadius, glm::vec3(1.0f, 0.0f, 0.0f));
		//scaling triangle down 
		model = glm::scale(model, glm::vec3(0.4f, 0.4f, 1.0f));
		//assign moving value to the shader we proivde the id or location of global value and the offset 
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		//attach prespective  
		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(camera.calculateViewMatrix()));
		brickTexture.UseTexture();
		meshList[0]->RenderMesh();

		//create another mesh
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(0.0f, 1.0f, -2.5f));
		model = glm::scale(model, glm::vec3(0.4f, 0.4f, 1.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		dirtTexture.UseTexture();
		meshList[1]->RenderMesh();
		//clear after using program
		glUseProgram(0);

		mainWindow.swapBuffers();
	}

	return 0;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
